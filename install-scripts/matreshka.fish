#! /usr/bin/fish

ada_install aws

cd ~/repos/Ada
git clone https://github.com/reznikmm/matreshka.git
cd matreshka

make all install PREFIX=~/Ada
