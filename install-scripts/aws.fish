#! /usr/bin/fish

ada_install xmlada

cd ~/repos/Ada
git clone --recursive https://github.com/AdaCore/aws.git
cd aws

make setup build install PREFIX=~/Ada
