#! /usr/bin/fish

cd ~/repos/Ada
git clone https://github.com/flyx/OpenGLAda.git
cd OpenGLAda

make WINDOWING_SYSTEM=x11 MODE=release
