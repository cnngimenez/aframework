#! /usr/bin/fish

# aframework.fish

# Copyright 2021 cnngimenez

# Author: cnngimenez

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

function ada__create_path --description="Create given path and show results."
    echo "Creating $argv[1]... "
    if mkdir -p ~/Ada
        set_color -o green
        echo "✓"
    else
        set_color -o red
        echo "✗"
    end
    set_color normal
end     

function ada_create_install_paths --description="Create a list of predefined paths for Ada and GNAT."
    set -l pathlist ~/Ada ~/repos/Ada

    for dir in pathlist
        ada__create_path "$dir"
    end
end

function ada_locate_default_ads
    ls -1d /usr/lib/gcc/x86_64-pc-linux-gnu/*/adainclude
end

function ada_link_to_default_ads
    set -l adainclude_path (ada_locate_default_ads)
    if test -L ~/Ada/adainclude
        rm -v ~/Ada/adainclude
    end 
    ln -vs "$adainclude_path" ~/Ada/adainclude    
end

function ada_update_include_path --description="Update ADA and GNAT variables."
    set -eg ADA_INCLUDE_PATH
    set -eg ADA_PROJECT_PATH
    set -eg GPR_PROJECT_PATH

    set -xU ADA_INCLUDE_PATH (find ~/Ada/include/ -type d) /usr/share/gpr/
    set -xU GPR_PROJECT_PATH ~/Ada/share/gpr /usr/share/gpr ~/Ada/lib/gnat

    echo "Add lib path to the ld. You may need to reset the LD_LIBRARY_PATH variable. Please take a look at its value!"

    set -xU LD_LIBRARY_PATH $LD_LIBRARY_PATH (find ~/Ada/lib -type d)

    set -U | egrep '(ADA|GPR)'
end

function ada_check_dependencies
  if string match '*.gpr' "$argv[1]"
      gprls -U -P "$argv[1]"
  else
      objdump -p "$argv[1]" | grep NEEDED
  end
end

#! /usr/bin/fish

function ada_is_installed --description="Is an Ada package installed?"
    set -l found (ls -1d "$HOME/repos/Ada/$argv[1]" 2> /dev/null)
    if test -z "$found"
        set found (gprinstall --list --prefix=$HOME/Ada | grep -o -w "$argv[1]")
        if test -n  "$found"
            set found "gprinstall --list returned: $found"
        end
    end
    if test -n "$found"
        echo "Installed at: $found"
    else
        echo "Not installed."
    end
end

function ada_clone --description "Clone the repository into the Ada repos folder and chdir it."
  set -l name (basename "$argv[1]" .git)

  cd ~/repos/Ada
  if test -d "$name"
      echo "$name has been already cloned."
  else
      git clone --recursive --depth=1 "$argv[1]"
  end
  cd "$name"
end

function ada_uninstall --description "Uninstall an Ada package."
    cd ~/repos/Ada/aframework/uninstall-scripts

    set -l script_name "$argv[1].fish"

    if test -f "$script_name"
        # The uninstall script exists, use it
        fish "$script_name"
    else
        gprinstall --uninstall "$argv[1]"
    end

    cd -
end

function ada_clean --description "Clean compiled files from an Ada package."
    set -l prev_dir (pwd)
    cd ~/repos/Ada/aframework/clean-scripts
    set -l script_name "$argv[1].fish"

    if test -f "$script_name"
        # Clean script found.
        fish "$script_name"
    else
        cd "$HOME/repos/Ada/$argv[1]"
        set -l cleanrule (grep "clean:" Makefile 2> /dev/null)

        if test -n "$cleanrule"
            # Makefile with clean rule...
            make clean              
        else
            if test -f "$argv[1].gpr"
                # GPRBuild project...
                gprclean -P"$argv[1].gpr"
            else
                set_color --bold red
                echo "aframework: I do not know how to clean this project!"
                set_color normal
                echo "aframework: No clean script, Makefile or GPRBuild file found!"
            end
        end
    end

    cd "$prev_dir"
end

function ada_install --description "Install an Ada package. 
Provide an install script name as argument. 
If second argument is \"reinstall\", then uninstall, clean and reinstall the package."
    cd ~/repos/Ada/aframework/install-scripts

    set -l installed (ada_is_installed "$argv[1]")

    if test -n "$installed"
        echo "$argv[1] is already installed."
        echo "$installed"
        if test "$argv[2]" = "reinstall"
            ada_uninstall "$argv[1]"
            ada_clean "$argv[1]"
            fish "$argv[1].fish"
        end
    else
        fish "$argv[1].fish"
    end

    cd - 
end
